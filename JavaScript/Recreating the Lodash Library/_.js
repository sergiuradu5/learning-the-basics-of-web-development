const _ = {
    clamp (number, lower, upper) {
      if(number<lower) return lower;
      else if(number>upper) return upper;
      else if(number>=lower && number <=upper) return number;
    },
    inRange (number, par1, par2)
    {
      let start;
      let end;
      //IF WE HAVE ONLY 2 ARGUMENTS
      if(typeof par2 === 'undefined')
        {
          start=0; end=par1;
        }
      else
      //IF WE HAVE 3 ARGUMENTS (PARAMETERS)
        //And they are swapped
      if(par1>par2)
        {
                start=par2;
          end=par1;
        }
      else {start = par1; end = par2;}
      
      //IS IT IN THE RANGE?
      if(number>=start && number<end) return true;
      else return false;
    },
    words(string) {
      let array = string.split(' ');
      return array;
    },
    pad(string, len)
    {
      if(string.length >= len)
        return string;
      else
        {
          let result = string;
          let padAmount = len - string.length;
          for(let i=0; i< Math.floor(padAmount/2); i++)
            {
              result = ' '+result;
            }
          for( i =0; i< Math.floor(padAmount/2); i++)
            {
              result=result+' ';
            }
          if(padAmount % 2 !== 0)
            result=result+' ';
          return result;
        }
    },
    has(obj, key)
    {
    
      if(obj[key] !== undefined)
        return true;
      else return false;
    },
    invert(obj)
    {
      let newObj;
      var key;
      for(key in obj)
        {
          let origValue = obj[key];
          newObj.origValue=key;
        }
      return newObj;
    },
    chunk(array, size)
    {
      if(typeof size === 'undefined')
        size=1;
      let newArray = [];
      let counter =1;
      let subArray = [];
      for(let i=0; i<array.length; i++)
        {
          
          if(counter % size !=0)
            {
              subArray.push(array[i]);
             
            }
          else
            {
              subArray.push(array[i]);
              newArray.push(subArray);
              subArray = [];
              
            }
          counter++;
        }
      if(subArray.length >= 1 && subArray !== undefined){
          newArray.push(subArray);
          }
      return newArray;
      
    }
  };
  
  
  
  
  // Do not write or modify code below this line.
  module.exports = _;