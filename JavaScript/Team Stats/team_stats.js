const team = {
  _players: [
    {
      firstName: 'Mark',
      lastName: 'Pomp',
      age: 34
    },
    {
    	firstName: 'Ralph',
      lastName: 'Lustling',
      age: 29
    },
    {
      firstName: 'James',
      lastName: 'DeFranco',
      age: 33
    }
  ],
  _games: [
    {
      opponent: 'Nachos',
      teamPoints: 45,
      opponentPoints: 42
    },
    {
      opponent: 'Crocs',
      teamPoints: 55,
      opponentPoints: 67
    },
    {
      opponent: 'Spartans',
      teamPoints: 28,
      opponentPoints: 49
    }
  ],
  
  get players () {
    return this._players;
  },
  get games () {
    return this._games;
  },
  addPlayer (firstName, lastName, age)
  {
    const player = {
      firstName: firstName,
      lastName: lastName,
      age: age
    }
    this._players.push(player);
  },
  addGame (opponent, teamPoints, opponentPoints) {
    const game = {
      opponent: opponent,
      teamPoints: teamPoints,
      opponentPoints: opponentPoints
    };
    this._games.push(game);
}
};

team.addPlayer('Steph', 'Curry', 28);
team.addPlayer('Lisa', 'Leslie', 44);
team.addPlayer('Bugs', 'Bunny', 76);

team.addGame('Golden State', 41, 92);
team.addGame('Lakers', 67, 59);
team.addGame('Boozers', 99, 43);
console.log(team._games);