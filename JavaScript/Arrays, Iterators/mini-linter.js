let story = 'Last weekend, I took literally the most beautiful bike ride of my life. The route is called "The 9W to Nyack" and it actually stretches all the way from Riverside Park in Manhattan to South Nyack, New Jersey. It\'s really an adventure from beginning to end! It is a 48 mile loop and it basically took me an entire day. I stopped at Riverbank State Park to take some extremely artsy photos. It was a short stop, though, because I had a really long way left to go. After a quick photo op at the very popular Little Red Lighthouse, I began my trek across the George Washington Bridge into New Jersey.  The GW is actually very long - 4,760 feet! I was already very tired by the time I got to the other side.  An hour later, I reached Greenbrook Nature Sanctuary, an extremely beautiful park along the coast of the Hudson.  Something that was very surprising to me was that near the end of the route you actually cross back into New York! At this point, you are very close to the end.';

let storyWords=story.split(' ');
console.log(storyWords.length);

let overusedWords = ['really', 'very', 'basically'];

let unnecessaryWords = ['extremely', 'literally', 'actually' ];

let betterWords = storyWords.filter(word=>{
  for(let i=0; i<unnecessaryWords.length;i++)
    {
      if(word.includes(unnecessaryWords[i]))
        return false;
    }
  		return true;
})
function countOverusedWords(string)
{
		let cnt1 =0; //really
  	let cnt2=0;  //very
    let cnt3=0;  //basically
		string.forEach(word=> {
  			  if(word.includes('really'))
            cnt1=cnt1+1;
          if(word.includes('very'))
            cnt2++;
      		if(word.includes('basically'))
            cnt3++;
      })
  console.log(`really appears ${cnt1} times`);
  console.log(`very appears ${cnt2} times`);
  console.log(`basically appears ${cnt3} times`);
}
countOverusedWords(storyWords);

function countSentences(array)
{
  let count=0;
  array.forEach(word=>{
    if(word.includes('.') || word.includes('!') || word.includes('?'))
       count++;
  })
    console.log(`There are ${count} sentences`);
}
countSentences(storyWords);

const betterStory=betterWords.join(' ');
console.log(betterStory);