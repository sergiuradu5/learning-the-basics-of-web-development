const menu = {
    _courses: {
        appetizers: [],
        mains: [],
      desserts: []
    },
   
    get appetizers (){
      return this._courses.appetizers;
    },
    get mains (){
      return this._courses.mains;
    },
    get desserts()
    {
      return this._courses.desserts;
    },
    set appetizers(a)
    {
      this._courses.appetizers=a;
    },
    set mains (m){
       this._courses.mains=m;
    },
    set desserts(d)
    {
     this._courses.desserts=(d);
    },
     get courses()
    {
      return {
                appetizers: this.appetizers,
                mains: this.mains,
                desserts: this.desserts
                  }
    },
    addDishToCourse(courseName, dishName, dishPrice) {
      const dish = {
        name: dishName,
        price: dishPrice
      }
       this._courses[courseName].push(dish);
    },
    getRandomDishFromCourse(courseName) {
      const dishes = this._courses[courseName];
      const urand = Math.floor(Math.random() * dishes.length);
      return dishes[urand];
    },
    generateRandomMeal() {
      let a = this.getRandomDishFromCourse('appetizers');
      let m = this.getRandomDishFromCourse('mains');
      let d = this.getRandomDishFromCourse('desserts');
      let totalPrice=a.price+m.price+d.price;
      return `${a.name}, ${m.name}, ${d.name}, Total price: ${totalPrice}`;
    }
  };
  
  menu.addDishToCourse('appetizers', 'Carbonara', 25);
  menu.addDishToCourse('appetizers', 'Salmon Bruschete', 21);
  menu.addDishToCourse('appetizers', 'Olive Bruschete', 17);
  
  menu.addDishToCourse('mains', 'Quattro Fromagi', 30);
  menu.addDishToCourse('mains', 'Shaorma', 24);
  menu.addDishToCourse('mains', 'Texas Steak', 38);
  
  menu.addDishToCourse('desserts', 'Cheese Cake', 15);
  menu.addDishToCourse('desserts', 'Lava Cake', 14);
  menu.addDishToCourse('desserts', 'Ice Cream', 12);
  
  let meal=menu.generateRandomMeal();
  console.log(meal);