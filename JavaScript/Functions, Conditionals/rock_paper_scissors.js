const getUserChoice = userInput => {
  userInput=userInput.toLowerCase();
    if (userInput === 'rock' || userInput === 'paper' || userInput === 'scissors' ) {
    return userInput;
  } else {
    console.log('Error!');
  }
};

const getComputerChoice = () =>
{
  const random =Math.floor(Math.random()*3);
  if(random===0) return 'rock';
  else if(random===1) return 'paper';
  else
    return 'scissors';
};

function determineWinner(userChoice, computerChoice)
{
  if(userChoice===computerChoice)
    return "It's a tie";
  else
    if(userChoice === 'rock')
      {
        if(computerChoice==='paper') return 'Computer wins';
      
  			else return 'User wins';
      }
  
  	else if(userChoice === 'paper')
      {
        if(computerChoice === 'scissors') return 'Computer wins';
        else return 'User wins';
      }
  
  	else if(userChoice === 'scissors')
      {
        if(computerChoice === 'rock') return 'Computer wins';
        else return 'User wins';
      }
}

function playGame()
{
  let userChoice = getUserChoice('scissors');
  let computerChoice = getComputerChoice();
  console.log(`User choice: ${userChoice}, Computer choice: ${computerChoice}`);
  console.log(determineWinner(userChoice, computerChoice));
}

playGame();