function getSleepHours(day)
{
  day=day.toLowerCase();
 if(day==='monday')
   return 8;
  else if (day === 'tuesday')
    return 7;
  else if (day === 'wednesday')
    return 7;
  else if (day === 'thursday')
    return 9;
  else if (day === 'friday')
    return 8;
  else if (day === 'saturday')
    return 9;
  else if (day === 'sunday')
    return 8;
  else return 'Argument is invalid!';
}

function getActualSleepHours()
{
  let sum=0;
  sum = sum + getSleepHours('monday');
  sum += getSleepHours('tuesday');
  sum += getSleepHours('wednesday');
  sum += getSleepHours('thursday');
  sum += getSleepHours('friday');
  sum += getSleepHours('saturday');
  sum += getSleepHours('sunday');
  return sum;
  
}

  function getIdealSleepHours()
  {
    const idealHours = 8;
    return idealHours*7;
  }

function calculateSleepDebt()
{
  let actualSleepHours = getActualSleepHours();
  let idealSleepHours = getIdealSleepHours();
  if(actualSleepHours === idealSleepHours)
    {
      console.log("Perfect amount of sleep");
    }
  	else if(actualSleepHours>idealSleepHours)
      console.log(`More sleep than needed. You had ${actualSleepHours-idealSleepHours} hours of extra sleep`);
  
  else console.log(`Get some rest. You have ${idealSleepHours - actualSleepHours} hours under the ideal sleep.`);
}
calculateSleepDebt();