export interface ICity {
        "cityId": number;
        "countyId": number;
        "longitude": number;
        "latitude": number;
        "cityName": string;
}