import { Injectable } from '@angular/core';
import { ICity } from './city';
import { Observable, throwError } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  private citiesUrl = 'api/cities.json';
  constructor(private http: HttpClient) { }

  getCities():Observable<ICity[]>   {
    return this.http.get<ICity[]>(this.citiesUrl).pipe(
      tap(data => console.log('ALL: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  private handleError(err: HttpErrorResponse)
  {
    let errorMessage = '';
    if(err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }


}
