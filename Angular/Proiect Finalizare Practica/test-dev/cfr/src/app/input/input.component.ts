import { Component, OnInit } from '@angular/core';
import { IPassenger } from './passenger/passenger';
import {Router} from '@angular/router';
import { CitiesService } from './city/cities.service';
import { ICity } from './city/city';
import { TicketService } from '../ticket/ticket.service';
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  departure: string;
  arrival: string;
  nrElev: number =0; //no of Passengers = elev TYPE 1
  nrAdult: number =0; //no of Passengers = adult TYPE 2
  nrPens: number=0; //no of Passengers = Pensionar TYPE 3
  arrPass: IPassenger[] = []; //an array of Passengers, each of them has a type
  arrCities: ICity[] =[];
  classType: number;
 // collected:boolean = false;
  errorMessage: string = "";

  constructor(private citiesService : CitiesService,
                  private ticketService: TicketService,
                  private router: Router) { }

  ngOnInit() {
    this.citiesService.getCities().subscribe({
      next: cities => { this.arrCities = cities; },
      error: err => this.errorMessage = err 
    });
    
  }

  saveDataInArrPass(): boolean {
    if( (this.nrElev <=0 && this.nrAdult <=0 && this.nrPens <=0))
      
    {this.errorMessage='E nevoie să introduceți cel puțin un pasager';
      console.log('Error! You have to introduce at least one Passanger.')
      return false;
    }
    if(this.classType===undefined) {
      this.errorMessage="E nevoie să selectați clasa dorită";
      console.log("Error! Class not selected!");
      return false;
    }
    if(this.nrElev> 0)
      for(let i=0; i<this.nrElev; i++)
      {
        this.arrPass.push(new IPassenger(1));
      }

    if(this.nrAdult > 0)
    for(let i=0; i<this.nrAdult; i++)
      {
        this.arrPass.push(new IPassenger(2));
      }

    if(this.nrPens > 0 )
    for(let i=0; i<this.nrPens; i++)
      {
        this.arrPass.push(new IPassenger(3));
      }
    console.log(`Data saved in array successfully!`);
    this.errorMessage = "";
    return true;
    
  }
  verifyCity() :boolean
  {
    if(this.departure===undefined)
    {
      this.errorMessage="Nu ați introdus localitatea de plecare!";
      return false;
    }
    if(this.arrival===undefined)
    {
      this.errorMessage="Nu ați introdus localitatea destinație!";
      return false;
    }
    else { this.errorMessage="";
      return true;}

  }
  onSubmit()
  {
    if(this.saveDataInArrPass() && this.verifyCity() )
    {
      this.ticketService.departureCity=this.findCity(this.departure);
      this.ticketService.arrivalCity=this.findCity(this.arrival);
      if(this.ticketService.arrivalCity!==undefined
        &&this.ticketService.arrivalCity!==undefined)
        {
        this.ticketService.distance=Math.floor(this.getDistance(this.ticketService.departureCity,
                                this.ticketService.arrivalCity)/1000);
        this.ticketService.arrPass=this.arrPass;
        this.ticketService.classType=this.classType;
        console.log(`Data submitted succesfully! TicketService: ${Object.entries(this.ticketService)}`);
        this.router.navigate(['/ticket']);
        }
    }
  }


  findCity(cityName : string) : ICity {
      cityName = cityName.charAt(0).toUpperCase()+cityName.slice(1);
      console.log(`The parameter edited is : ${cityName}`);
      let foundCity : ICity;
      foundCity = this.arrCities.find(elem => {
        
        return elem.cityName===cityName;
      });
      if(foundCity === undefined) {
        console.log(`Error: The city was not found!`);
      }
      else {
        console.log(`The city ${foundCity.cityName} was found!`);
        return foundCity;}
  }

  rad  (x:number) :number {
    return x * Math.PI / 180;
  };
  
  getDistance (p1 : ICity, p2:ICity ) :number {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = this.rad(p2.latitude - p1.latitude);
    var dLong = this.rad(p2.longitude - p1.longitude);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.latitude)) * Math.cos(this.rad(p2.latitude)) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
  };

}
