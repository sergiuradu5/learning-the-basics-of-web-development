import { IPassenger } from '../input/passenger/passenger';
import { ICity } from '../input/city/city';

export class TicketClass {
    "trainId": string;
    "distance": number;
    "vagon":number;
    "seat": number;
    "ticketId":string;
    "price":number;
    "arrPass":IPassenger[];
    "classType":number;
    "departureCity":ICity;
    "arrivalCity":ICity;
    "nrPass":number;
}