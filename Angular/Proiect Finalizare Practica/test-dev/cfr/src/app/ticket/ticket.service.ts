import { Injectable } from '@angular/core';
import { ICity } from '../input/city/city';
import { TicketClass } from './ticket.class';
import { IPassenger } from '../input/passenger/passenger';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  departureCity: ICity;
  arrivalCity: ICity;
  distance: number;
  arrPass: IPassenger[];
  classType: number;
  constructor() { }

  calculatePrice():number {
    let initPrice: number=0;
    let finalPrice: number=0;
    initPrice= this.classType===1 ? this.distance*0.3214 : this.distance * 0.2543;
    for(let i =0; i<this.arrPass.length; i++)
    {
      if(this.arrPass[i].getDiscountType()===1)
      {
        finalPrice += initPrice/2;
      }
      else if (this.arrPass[i].getDiscountType()===2)
      {
        finalPrice += initPrice;
      }
      else if (this.arrPass[i].getDiscountType()===3)
      {
        finalPrice += initPrice/4;
      }
    }
    return finalPrice;
  }

  generateTicket():TicketClass {
    let ticket : TicketClass = new TicketClass();
    ticket.trainId = (this.departureCity.longitude % 1).toString().charAt(2)+
                      (this.departureCity.latitude % 1).toString().charAt(2) +
                      (this.arrivalCity.longitude % 1).toString().charAt(2)+
                      (this.arrivalCity.latitude % 1).toString().charAt(2);
    ticket.distance = this.distance;
    ticket.vagon = Math.ceil(Math.random()*2);
    ticket.seat = Math.ceil(Math.random()*87);

    let sum:string = this.departureCity.cityName.substring(0,2).toUpperCase() + 
    (this.departureCity.longitude %1).toString().substring(2,4) +
    (this.departureCity.latitude %1).toString().substring(2,4) +
    this.arrivalCity.cityName.substring(0,2).toUpperCase() + 
    (this.arrivalCity.longitude %1).toString().substring(2,4) +
    (this.arrivalCity.latitude %1).toString().substring(2,4);
    
    let hexaNum : number = 0;
      for(let i =0; i<12; i++)
      {
          hexaNum += sum.charCodeAt(i);
      }
    ticket.ticketId = sum + hexaNum.toString(16);
    ticket.arrPass = this.arrPass;
    ticket.classType = this.classType;
    ticket.price = this.calculatePrice();
    ticket.arrivalCity=this.arrivalCity;
    ticket.departureCity=this.departureCity;
    ticket.nrPass=this.arrPass.length;
    return ticket;
  }
}
