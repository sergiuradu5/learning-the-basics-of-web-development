import { Component, OnInit } from '@angular/core';
import { TicketService } from './ticket.service';
import { TicketClass } from './ticket.class';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  ticket : TicketClass = new TicketClass();

  constructor(private ticketService : TicketService,
    private route: ActivatedRoute) { 
    this.ticket = this.ticketService.generateTicket();
    console.log(`Ticket generated successfully!: ${this.ticket}`);
  }

  ngOnInit() {

  }

}
