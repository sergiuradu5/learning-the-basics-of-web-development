import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputModule } from './input/input.module';
import { TicketModule } from './ticket/ticket.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputModule,
    TicketModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
