window.onload = function() {

    function generateId(replaceIdText) {
        let id = '';
        let charsNums = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for(var i = 0; i < 12; i++) {
            id += charsNums.charAt(Math.floor(Math.random() * charsNums.length));
        }
        
        if(replaceIdText === true) {
            document.querySelector(".js-ticket-id").innerHTML = id;
        }

        return id;
    }
    
    function generateQrCode(id)
    {
        $(".js-qrcode").qrcode({ width: 126, height: 126, text: id });
    }

    function resizeCoordinatesFont() {
        let departure = document.querySelector(".js-departure");
        let destination = document.querySelector(".js-destination");
        
        // If either one of the coordinates titles overflows on a second line, make the other overflow too or
        if(departure.offsetHeight > 19) {
            destination.style.wordSpacing = "100px";
        } else if (destination.offsetHeight > 19) {
            departure.style.wordSpacing = "100px";
        }
    }
    
    // Function Calls
    generateId(true);

    generateQrCode(generateId(false));
    
    resizeCoordinatesFont();
    
}